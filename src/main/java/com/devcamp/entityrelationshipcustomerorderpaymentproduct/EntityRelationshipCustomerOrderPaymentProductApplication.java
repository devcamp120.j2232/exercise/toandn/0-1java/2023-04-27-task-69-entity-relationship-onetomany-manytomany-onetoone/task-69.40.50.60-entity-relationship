package com.devcamp.entityrelationshipcustomerorderpaymentproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntityRelationshipCustomerOrderPaymentProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntityRelationshipCustomerOrderPaymentProductApplication.class, args);
	}

}
