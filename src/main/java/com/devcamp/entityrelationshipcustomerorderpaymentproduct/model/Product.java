package com.devcamp.entityrelationshipcustomerorderpaymentproduct.model;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "price")
  private long price;

  @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
  @JoinTable(name = "product_order", joinColumns = { @JoinColumn(name = "product_id") }, inverseJoinColumns = {
      @JoinColumn(name = "order_id") })
  private Set<Order> orders;

  public Product() {
  }

  public Product(long id, String name, long price, Set<Order> orders) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.orders = orders;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getPrice() {
    return price;
  }

  public void setPrice(long price) {
    this.price = price;
  }

  public Set<Order> getOrders() {
    return orders;
  }

  public void setOrders(Set<Order> orders) {
    this.orders = orders;
  }

}
