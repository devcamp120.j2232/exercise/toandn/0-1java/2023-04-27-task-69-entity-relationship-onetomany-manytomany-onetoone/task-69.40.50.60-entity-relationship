package com.devcamp.entityrelationshipcustomerorderpaymentproduct.model;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "payments")
public class Payment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "totalPrice")
  private long totalPrice;

  @Column(name = "status")
  private String status;

  @Column(name = "address")
  private String address;

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "order_id", nullable = false)
  private Order order;

  public Payment() {
  }

  public Payment(long id, long totalPrice, String status, String address, Order order) {
    this.id = id;
    this.totalPrice = totalPrice;
    this.status = status;
    this.address = address;
    this.order = order;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(long totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

}
